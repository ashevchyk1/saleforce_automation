#!/usr/bin/env bash
sfdx force:auth:jwt:grant -d -a Hub -i $CONSUMER_KEY -f assets/server.key -u $USERNAME
sfdx force:org:create -a $SCRATCH_ORG_ALIAS -s edition=Developer
mkdir app_source
cd app_source
git clone -q https://gitlab-ci-token:$CI_TOKEN@gitlab.com/mmorozov/element-ar-poc.git .
git checkout f556170d246cc6037be311b12cb0218c4435eb08 &>/dev/null
sfdx force:source:push &>/dev/null
cd ..
sfdx force:user:permset:assign -n AR_Admin
sfdx force:org:list