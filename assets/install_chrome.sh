#!/usr/bin/env bash
CHROME_VERSION=google-chrome-stable
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add &>/dev/null
echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list
apt-get update &>/dev/null
apt-get install -y ${CHROME_VERSION} &>/dev/null
google-chrome --version