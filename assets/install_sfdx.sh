#!/usr/bin/env bash
apt-get update &>/dev/null
apt-get install -y wget xz-utils &>/dev/null
wget https://developer.salesforce.com/media/salesforce-cli/sfdx-linux-amd64.tar.xz &>/dev/null
mkdir sfdx
tar xJf sfdx-linux-amd64.tar.xz -C sfdx --strip-components 1
./sfdx/install
