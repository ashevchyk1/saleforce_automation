package enums;

public enum AggregatedReportPeriod {
    PAST_SIX_MONTH("Past 6 Months"),
    PAST_YEAR("Past Year");

    private final String periodOption;

    AggregatedReportPeriod(String periodOption){
        this.periodOption = periodOption;
    }

    public String getPeriodOption(){
        return periodOption;
    }
}