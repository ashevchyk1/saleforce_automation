package enums;

public enum AggregatedReportRecurrence {
    QUARTERLY("Quarterly"),
    SEMI_ANNUALLY("Semi-annually"),
    ANNUALLY("Annually");

    private final String recurrenceOption;

    AggregatedReportRecurrence(String recurrenceOption){
        this.recurrenceOption = recurrenceOption;
    }

    public String getRecurrenceOption(){
        return recurrenceOption;
    }
}