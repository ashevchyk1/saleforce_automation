package models;

import enums.AggregatedReportPeriod;
import enums.AggregatedReportRecurrence;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class AggregateReportSchedulerModel {
    private String reportName;
    private AggregatedReportRecurrence recurrence;
    private AggregatedReportPeriod period;
    private String searchProduct;
    private ProductModel productModel;
    private ReportTypeModel reportTypeModel;
    @Builder.Default
    private boolean successCreation = true;
}