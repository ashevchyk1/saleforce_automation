package web;

import domain.web.businesslayer.sf.AuthenticationBusinessLayer;
import models.AggregateReportSchedulerModel;
import models.AuthenticationModel;

import static com.codeborne.selenide.Selenide.open;
import static domain.web.businesslayer.aggregatedreport.AggregateReportSchedulerBusinessLayer.createAggregateReportScheduler;
import static domain.web.businesslayer.sf.SalesforceSetupBusinessLayer.openAggregatedReports;
import static java.lang.System.getProperty;
import static org.assertj.core.api.Assertions.assertThat;

public class AggregateReportingBaseWebTest extends BaseWebTest {

    protected void testMe(AggregateReportSchedulerModel aggregateReportSchedulerModel) {
        openAggregatedReport();
        createAggregateReportScheduler(aggregateReportSchedulerModel);
    }

    private void openAggregatedReport() {
        openSalesforce();
        openAggregatedReports();
    }

    private void openSalesforce() {
        String sfdxUrl = getProperty("sfdx.url");
        if (sfdxUrl != null) {
            System.out.println("sfdx url = " + sfdxUrl);
            open(sfdxUrl);
        } else {
            String sfUrl = getProperty("sf.url");
            String username = getProperty("sf.username");
            String pass = getProperty("sf.pass");
            assertThat(sfUrl).isNotNull();
            assertThat(username).isNotNull();
            assertThat(pass).isNotNull();
            open(sfUrl);
            AuthenticationModel authenticationModel = AuthenticationModel.builder()
                    .username(username)
                    .password(pass)
                    .build();
            AuthenticationBusinessLayer.authenticate(authenticationModel);
        }
    }
}