package web;

import engine.DriverConfigurator;
import engine.DriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import utils.ScreenshotListener;

@Listeners(ScreenshotListener.class)
public class BaseWebTest {

    @BeforeSuite(description = "Initialize basic configuration")
    protected void beforeSuite() {
        DriverConfigurator.configure();//don't start driver
    }

    @AfterMethod(alwaysRun = true, description = "Close driver")
    protected void tearDown() {
        DriverUtils.stop();
    }
}
