package web;

import enums.AggregatedReportPeriod;
import enums.AggregatedReportRecurrence;
import models.AggregateReportSchedulerModel;
import models.ProductModel;
import models.ReportTypeModel;
import org.testng.annotations.Test;

import java.util.Random;

public class UnsuccessfullyAggregateReportSchedulerCreationTest extends AggregateReportingBaseWebTest {

    @Test
    public void testUnsuccessfullyAggregateReportSchedulerCreation() {
        ProductModel productModel = ProductModel.builder()
                .productName("product name" + new Random().nextInt())
                .build();
        ReportTypeModel reportTypeModel = ReportTypeModel.builder()
                .reportType("report type" + new Random().nextInt())
                .build();
        AggregateReportSchedulerModel aggregateReportSchedulerModel = AggregateReportSchedulerModel.builder()
                .reportTypeModel(reportTypeModel)
                .productModel(productModel)
                .recurrence(AggregatedReportRecurrence.ANNUALLY)
                .period(AggregatedReportPeriod.PAST_SIX_MONTH)
                .build();

        testMe(aggregateReportSchedulerModel);
    }
}
