package domain.web.businesslayer.aggregatedreport;

import domain.web.businesslayer.AbstractBusinessLayer;
import domain.web.frontendlayer.overlays.ar.ReportOverlay;
import domain.web.frontendlayer.pages.ar.AggregatedReportsSchedulerPage;
import io.qameta.allure.Step;
import models.AggregateReportSchedulerModel;

import java.util.HashMap;
import java.util.Map;

import static com.codeborne.selenide.Condition.visible;
import static domain.web.businesslayer.aggregatedreport.ProductsBusinessLayer.*;
import static domain.web.businesslayer.aggregatedreport.ReportTypesBusinessLayer.*;
import static domain.web.frontendlayer.pages.ar.AggregatedReportsSchedulerPage.AggregatedReportsSchedulerTableColumns;
import static java.util.Optional.of;
import static java.util.Optional.ofNullable;

public class AggregateReportSchedulerBusinessLayer extends AbstractBusinessLayer {

    @Step
    public static void createAggregateReportScheduler(AggregateReportSchedulerModel aggregatedReport) {
        //create product
        createProduct(aggregatedReport.getProductModel());
        //create report type
        createReportType(aggregatedReport.getReportTypeModel());
        //got to aggregate report scheduler page
        ReportOverlay reportOverlay = new AggregatedReportsSchedulerPage()
                .globalHeader.openAggregatedReportsSchedulerPage()
                .openNewReport();
        //set report name
        ofNullable(aggregatedReport.getReportName()).ifPresent(reportOverlay::setReportName);
        //set report type
        ofNullable(aggregatedReport.getReportTypeModel()).ifPresent(s -> reportOverlay.selectReporType(s.getReportType()));
        //set product name
        ofNullable(aggregatedReport.getProductModel()).ifPresent(s -> reportOverlay.selectProduct(s.getProductName()));
        //select recurrence
        ofNullable(aggregatedReport.getRecurrence()).ifPresent(reportOverlay::selectRecurrence);
        //select period
        ofNullable(aggregatedReport.getPeriod()).ifPresent(reportOverlay::selectPeriod);
        //save aggregate report scheduler
        if (!aggregatedReport.isSuccessCreation()){
            reportOverlay.save();
            reportOverlay.verifyErrorMessage(visible);
        } else {
            reportOverlay.save().verifyAggregatedReportScheduler(generateReportMapValues(aggregatedReport));
        }
    }

    private static Map<AggregatedReportsSchedulerTableColumns, String> generateReportMapValues(AggregateReportSchedulerModel aggregatedReport) {
        var values = new HashMap<AggregatedReportsSchedulerTableColumns, String>();
        of(aggregatedReport.getReportName())
                .ifPresent(v -> values.put(AggregatedReportsSchedulerTableColumns.REPORT_NAME, v));
        of(aggregatedReport.getReportTypeModel().getReportType())
                .ifPresent(v -> values.put(AggregatedReportsSchedulerTableColumns.REPORT_TYPE, v));
        of(aggregatedReport.getProductModel().getProductName())
                .ifPresent(v -> values.put(AggregatedReportsSchedulerTableColumns.PRODUCT, v));
        of(aggregatedReport.getRecurrence().getRecurrenceOption())
                .ifPresent(v -> values.put(AggregatedReportsSchedulerTableColumns.RECURRENCE, v));
        of(aggregatedReport.getPeriod().getPeriodOption())
                .ifPresent(v -> values.put(AggregatedReportsSchedulerTableColumns.PERIOD, v));
        return values;
    }
}