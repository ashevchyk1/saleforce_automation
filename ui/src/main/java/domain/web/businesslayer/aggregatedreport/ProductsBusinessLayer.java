package domain.web.businesslayer.aggregatedreport;

import domain.web.businesslayer.AbstractBusinessLayer;
import domain.web.frontendlayer.pages.ar.ProductsPage;
import io.qameta.allure.Step;
import models.ProductModel;

public class ProductsBusinessLayer extends AbstractBusinessLayer {

    @Step
    public static void createProduct(ProductModel productModel) {
        if(productModel != null){
            new ProductsPage()
                    .globalHeader.openProductsPage()
                    .clickNew()
                    .setProductName(productModel.getProductName())
                    .save()
                    .verifyProductName(productModel.getProductName());
        }
    }
}
