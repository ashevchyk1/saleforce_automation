package domain.web.businesslayer.aggregatedreport;

import domain.web.businesslayer.AbstractBusinessLayer;
import domain.web.frontendlayer.pages.ar.ReportTypesPage;
import io.qameta.allure.Step;
import models.ReportTypeModel;

public class ReportTypesBusinessLayer extends AbstractBusinessLayer {

    @Step
    public static void createReportType(ReportTypeModel reportTypeModel) {
        if (reportTypeModel != null) {
            new ReportTypesPage()
                    .globalHeader.openReportTypesPage()
                    .clickNew()
                    .setReportType(reportTypeModel.getReportType())
                    .save()
                    .verifyReportTypeName(reportTypeModel.getReportType());
        }
    }

}
