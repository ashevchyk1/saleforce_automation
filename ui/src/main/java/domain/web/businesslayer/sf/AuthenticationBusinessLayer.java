package domain.web.businesslayer.sf;

import domain.web.businesslayer.AbstractBusinessLayer;
import domain.web.frontendlayer.pages.sf.LoginPage;
import models.AuthenticationModel;

public class AuthenticationBusinessLayer extends AbstractBusinessLayer {

    public static void authenticate(AuthenticationModel authenticationModel) {
        new LoginPage()
                .setName(authenticationModel)
                .setPassword(authenticationModel)
                .clickSingIn();
    }

}
