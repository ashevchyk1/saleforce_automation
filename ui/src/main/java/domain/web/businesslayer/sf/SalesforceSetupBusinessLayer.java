package domain.web.businesslayer.sf;

import domain.web.businesslayer.AbstractBusinessLayer;
import domain.web.frontendlayer.pages.sf.SetupHomePage;

public class SalesforceSetupBusinessLayer extends AbstractBusinessLayer {

    public static void openAggregatedReports(){
        new SetupHomePage()
                .globalHeader.openAppLauncher()
                .openAggregatedReportsScheduler();
    }
}