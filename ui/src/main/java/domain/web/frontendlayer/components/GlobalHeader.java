package domain.web.frontendlayer.components;

import com.codeborne.selenide.SelenideElement;
import domain.web.frontendlayer.overlays.AppLauncherOverlay;
import domain.web.frontendlayer.pages.ar.AggregatedReportsSchedulerPage;
import domain.web.frontendlayer.pages.ar.ProductsPage;
import domain.web.frontendlayer.pages.ar.ReportTypesPage;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class GlobalHeader extends AbstractComponent {
    private SelenideElement parent = $("#oneHeader"),
            appLauncherBtn = parent.$(".appLauncher"),
            navigation = parent.$(".navCenter ul");

    public AppLauncherOverlay openAppLauncher() {
        appLauncherBtn.click();
        return new AppLauncherOverlay();
    }

    public ProductsPage openProductsPage() {
        return openTab(new ImmutablePair<>(ProductsPage.PAGE_TITLE, new ProductsPage()));
    }

    public ReportTypesPage openReportTypesPage() {
        return openTab(new ImmutablePair<>(ReportTypesPage.PAGE_TITLE, new ReportTypesPage()));
    }

    public AggregatedReportsSchedulerPage openAggregatedReportsSchedulerPage() {
        return openTab(new ImmutablePair<>(AggregatedReportsSchedulerPage.PAGE_TITLE, new AggregatedReportsSchedulerPage()));
    }

    private boolean isTabSelected(String tab) {
        return getTab(tab).getCssValue("class").contains("active");
    }

    private <T> T openTab(Pair<String, T> tab) {
        if (!isTabSelected(tab.getLeft())) {
            getTab(tab.getLeft()).click();
        }
        return tab.getRight();
    }

    private SelenideElement getTab(String tab) {
        return navigation.$$("li > a").filter(visible).filter(exactText(tab)).shouldHave(size(1)).get(0);
    }
}