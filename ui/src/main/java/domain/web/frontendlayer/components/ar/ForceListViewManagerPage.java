package domain.web.frontendlayer.components.ar;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import domain.web.frontendlayer.pages.AbstractPage;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Selenide.$$;

public class ForceListViewManagerPage extends AbstractPage {
    private static final String NEW_BTN_TITLE = "New";
    private SelenideElement parent;
    private ElementsCollection brandingActions;

    protected ForceListViewManagerPage(String pageTitle) {
        this.parent = $$(".forceListViewManager .slds-breadcrumb__item").find(exactText(pageTitle)).closest(".windowViewMode-normal");
        this.brandingActions = this.parent.$(".branding-actions").$$("li");
    }

    protected void clickOnNewButton() {
        brandingActions.find(exactText(NEW_BTN_TITLE)).click();
    }
}
