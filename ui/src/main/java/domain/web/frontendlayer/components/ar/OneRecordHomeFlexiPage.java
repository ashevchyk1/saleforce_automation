package domain.web.frontendlayer.components.ar;

import com.codeborne.selenide.SelenideElement;
import domain.web.frontendlayer.pages.AbstractPage;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Selenide.$$;

public class OneRecordHomeFlexiPage extends AbstractPage {
    private SelenideElement parent, headerTitle;

    protected OneRecordHomeFlexiPage(String pageTitle) {
        this.parent = $$(".oneRecordHomeFlexipage .slds-breadcrumb__item").find(exactText(pageTitle)).closest(".windowViewMode-normal");
        this.headerTitle = parent.$(".slds-page-header__title");
    }

    protected void verifyHeaderTitle(String title) {
        headerTitle.should(exactText(title));
    }
}
