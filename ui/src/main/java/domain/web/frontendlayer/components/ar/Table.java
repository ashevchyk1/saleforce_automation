package domain.web.frontendlayer.components.ar;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import domain.web.frontendlayer.pages.AbstractPage;
import io.qameta.allure.Step;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$$;
import static org.assertj.core.api.Fail.fail;

public class Table extends AbstractPage {
    private SelenideElement parent, table;

    public Table(String pageTitle) {
        this.parent = $$(".slds-text-heading_small").find(exactText(pageTitle)).closest(".windowViewMode-normal");
        this.table = this.parent.$("table");
    }

    @Step("Get table rows")
    private ElementsCollection rows() {
        return table.$("tbody").$$("tr").filter(visible);
    }

    @Step("Get table columns")
    private ElementsCollection columns() {
        return table.$("thead").$$("th").filter(visible).shouldHave(sizeGreaterThan(0));
    }

    @Step("Get column index by name '{columnName'}")
    private int getColumnIndex(String columnName) {
        var columns = columns();
        return IntStream.range(0, columns.size())
                .filter(i -> columns.get(i).getText().equals(columnName))
                .findFirst()
                .orElseThrow();
    }

    @Step("Check that row contains '{values}' in appropriate columns '{columnNames}'")
    public void verifyColumnValues(List<String> columnNames, List<String> values) {
        var columnsIndex = columnNames.stream()
                .map(this::getColumnIndex)
                .collect(Collectors.toList());
        var rows = rows();
        for (int i = 0; i < rows.size(); i++) {
            var row = rows.get(i);
            var cells = row.$$("td").texts();
            for (int j = 0; j < columnsIndex.size(); j++) {
                if (!cells.get(columnsIndex.get(j)).equals(values.get(j))) {
                    break;
                }
                if (j == columnsIndex.size() - 1) {
                    return;
                }
            }
        }
        fail("There are no rows with provided set of columns and values");
    }
}