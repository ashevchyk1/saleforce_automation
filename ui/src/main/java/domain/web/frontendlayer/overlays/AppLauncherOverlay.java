package domain.web.frontendlayer.overlays;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import domain.web.frontendlayer.pages.ar.AggregatedReportsSchedulerPage;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Selenide.$;


public class AppLauncherOverlay extends AbstractOverlay {
    private static final String AGGREGATE_REPORT_APP = "Aggregate Report";
    private SelenideElement app_cards = $("#cards");
    private ElementsCollection app_titles = app_cards.$$("#appTile");

    public AggregatedReportsSchedulerPage openAggregatedReportsScheduler() {
        app_titles.find(exactText(AGGREGATE_REPORT_APP)).click();
        return new AggregatedReportsSchedulerPage();
    }
}