package domain.web.frontendlayer.overlays.ar;

import domain.web.frontendlayer.pages.ar.ProductPage;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;

import static domain.web.frontendlayer.overlays.ar.SldsOverlay.SldsOverlayElementTypes.INPUT;

public class NewProductOverlay extends SldsOverlay {
    private Pair<SldsOverlayElementTypes, String> productNameInput = new ImmutablePair<>(INPUT, "Product Name");

    public NewProductOverlay setProductName(String productName) {
        fillInfo(Map.of(productNameInput, productName));
        return this;
    }

    public ProductPage save() {
        clickSave();
        return new ProductPage();
    }
}
