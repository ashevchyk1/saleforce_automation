package domain.web.frontendlayer.overlays.ar;

import domain.web.frontendlayer.pages.ar.ReportTypePage;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;

import static domain.web.frontendlayer.overlays.ar.SldsOverlay.SldsOverlayElementTypes.INPUT;

public class NewReportTypeOverlay extends SldsOverlay {
    private Pair<SldsOverlayElementTypes, String> reportTypeInput = new ImmutablePair<>(INPUT, "Report Type Name");

    public NewReportTypeOverlay setReportType(String reportType) {
        fillInfo(Map.of(reportTypeInput, reportType));
        return this;
    }

    public ReportTypePage save() {
        clickSave();
        return new ReportTypePage();
    }
}
