package domain.web.frontendlayer.overlays.ar;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import domain.web.frontendlayer.overlays.AbstractOverlay;
import domain.web.frontendlayer.pages.ar.AggregatedReportsSchedulerPage;
import enums.AggregatedReportPeriod;
import enums.AggregatedReportRecurrence;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Selenide.$$;

public class ReportOverlay extends AbstractOverlay {
    private SelenideElement parent = $(".modal-body .slds-card"),
            reportTypeCombobox = parent.$$("lightning-grouped-combobox > label").find(exactText("Report Type")).parent(),
            productCombobox = parent.$$("lightning-grouped-combobox > label").find(exactText("Product")).parent(),
            recurrence = parent.$$("lightning-combobox > label").find(exactText("Recurrence")).parent(),
            reportNameInput = parent.$(byName("Name")),
            period = parent.$(byName("Period__c")),
            saveButton = parent.$(byText("Save")),
            cancelButton = parent.$(byText("Cancel")),
            errorMessage = parent.$("lightning-messages");

    private ElementsCollection option = $$("lightning-base-combobox-formatted-text");

    @Step("Set report name '{reportName}'")
    public ReportOverlay setReportName(String reportName) {
        reportNameInput.setValue(reportName);
        return this;
    }

    @Step("Select recurrence '{reportRecurrence}'")
    public ReportOverlay selectRecurrence(AggregatedReportRecurrence reportRecurrence) {
       selectLightningComboboxItem(recurrence, reportRecurrence.getRecurrenceOption());
        return this;
    }

    @Step("Select period '{reportPeriod}'")
    public ReportOverlay selectPeriod(AggregatedReportPeriod reportPeriod) {
        period.click();
        option.find(exactText(reportPeriod.getPeriodOption())).click();
        return this;
    }

    @Step("Select product '{productName}'")
    public ReportOverlay selectProduct(String productName) {
        selectLightningComboboxItem(productCombobox, productName);
        return this;
    }

    @Step("Select report type '{reportType}'")
    public ReportOverlay selectReporType(String reportType) {
        selectLightningComboboxItem(reportTypeCombobox, reportType);
        return this;
    }

    @Step("Cancel create report")
    public AggregatedReportsSchedulerPage cancel() {
        cancelButton.click();
        return new AggregatedReportsSchedulerPage();
    }

    @Step("Create report")
    public AggregatedReportsSchedulerPage save() {
        saveButton.click();
        return new AggregatedReportsSchedulerPage();
    }

    @Step("Verify error message is '{condition}'")
    public ReportOverlay verifyErrorMessage(Condition condition){
        errorMessage.shouldBe(condition);
        return this;
    }

    private void selectLightningComboboxItem(SelenideElement e, String item) {
        e.$("input").click();
        e.$$("div lightning-base-combobox-formatted-text").find(exactText(item)).click();
    }
}