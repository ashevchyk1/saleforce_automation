package domain.web.frontendlayer.overlays.ar;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import domain.web.frontendlayer.overlays.AbstractOverlay;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static org.assertj.core.api.Assertions.fail;

public class SldsOverlay extends AbstractOverlay {
    private static final String SAVE_BTN_TITLE = "Save";
    private SelenideElement parent;
    private ElementsCollection footerBtn;

    protected SldsOverlay() {
        this.parent = $(".slds-modal__container");
        this.footerBtn = this.parent.$$(".modal-footer .slds-button");
    }

    protected void fillInfo(Map<Pair<SldsOverlayElementTypes, String>, String> uiElements) {
        uiElements.forEach((k, v) -> {
            SelenideElement label = parent.$$("label > span").find(exactText(k.getRight())).should(visible);
            switch (k.getLeft()) {
                case INPUT:
                    label.parent().parent().$("input").setValue(v);
                    break;
                case AUTOCOMPLETE_INPUT:
                    fail("TODO");
                    break;
                default:
                    fail("TODO");
            }
        });
    }

    protected void clickSave() {
        footerBtn.find(exactText(SAVE_BTN_TITLE)).click();
    }

    public enum SldsOverlayElementTypes {
        INPUT, AUTOCOMPLETE_INPUT;
    }
}
