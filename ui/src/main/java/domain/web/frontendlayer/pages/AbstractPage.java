package domain.web.frontendlayer.pages;

import domain.web.frontendlayer.components.GlobalHeader;

public abstract class AbstractPage {
    public final GlobalHeader globalHeader = new GlobalHeader();
}
