package domain.web.frontendlayer.pages.ar;

import com.codeborne.selenide.SelenideElement;
import domain.web.frontendlayer.components.ar.Table;
import domain.web.frontendlayer.overlays.ar.ReportOverlay;
import domain.web.frontendlayer.pages.AbstractPage;
import io.qameta.allure.Step;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class AggregatedReportsSchedulerPage extends AbstractPage {
    public final static String PAGE_TITLE = "Aggregated Reports Scheduler";
    private Table table = new Table(PAGE_TITLE);
    private SelenideElement parent = $("article.slds-card"),
            newButton = parent.find(byText("New"));

    @Step("Open new report overlay")
    public ReportOverlay openNewReport() {
        newButton.click();
        return new ReportOverlay();
    }

    @Step("Check Aggregated Report Scheduler values")
    public AggregatedReportsSchedulerPage verifyAggregatedReportScheduler(Map<AggregatedReportsSchedulerTableColumns, String> scheduler) {
        List<String> columns = scheduler.keySet().stream().map(AggregatedReportsSchedulerTableColumns::getColumn).collect(Collectors.toList());
        List<String> values = new ArrayList<>(scheduler.values());
        table.verifyColumnValues(columns, values);
        return this;
    }

    public enum AggregatedReportsSchedulerTableColumns {
        REPORT_NAME("REPORT NAME"), REPORT_TYPE("REPORT TYPE"), PRODUCT("PRODUCT"), RECURRENCE("RECURRENCE"),
        PERIOD("PERIOD"), LAST_RUN("LAST RUN"), STATUS("STATUS");

        private final String column;

        AggregatedReportsSchedulerTableColumns(String column) {
            this.column = column;
        }

        public String getColumn() {
            return column;
        }
    }
}