package domain.web.frontendlayer.pages.ar;

import domain.web.frontendlayer.components.ar.OneRecordHomeFlexiPage;

public class ProductPage extends OneRecordHomeFlexiPage {
    private static final String PAGE_TITLE = "Product";

    public ProductPage() {
        super(PAGE_TITLE);
    }

    public ProductPage verifyProductName(String productName) {
        verifyHeaderTitle(productName);
        return this;
    }
}
