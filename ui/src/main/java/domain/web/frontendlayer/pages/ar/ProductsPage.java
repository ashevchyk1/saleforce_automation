package domain.web.frontendlayer.pages.ar;

import domain.web.frontendlayer.components.ar.ForceListViewManagerPage;
import domain.web.frontendlayer.overlays.ar.NewProductOverlay;

public class ProductsPage extends ForceListViewManagerPage {
    public static final String PAGE_TITLE = "Products";

    public ProductsPage() {
        super(PAGE_TITLE);
    }

    public NewProductOverlay clickNew() {
        clickOnNewButton();
        return new NewProductOverlay();
    }
}
