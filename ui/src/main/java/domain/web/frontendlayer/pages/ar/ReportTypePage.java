package domain.web.frontendlayer.pages.ar;

import domain.web.frontendlayer.components.ar.OneRecordHomeFlexiPage;

public class ReportTypePage extends OneRecordHomeFlexiPage {
    private static final String PAGE_TITLE = "Report Type";

    public ReportTypePage() {
        super(PAGE_TITLE);
    }

    public ReportTypePage verifyReportTypeName(String reportType) {
        verifyHeaderTitle(reportType);
        return this;
    }
}
