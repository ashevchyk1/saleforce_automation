package domain.web.frontendlayer.pages.ar;

import domain.web.frontendlayer.components.ar.ForceListViewManagerPage;
import domain.web.frontendlayer.overlays.ar.NewReportTypeOverlay;

public class ReportTypesPage  extends ForceListViewManagerPage {
    public static final String PAGE_TITLE = "Report Types";

    public ReportTypesPage() {
        super(PAGE_TITLE);
    }

    public NewReportTypeOverlay clickNew() {
        clickOnNewButton();
        return new NewReportTypeOverlay();
    }
}
