package domain.web.frontendlayer.pages.sf;

import com.codeborne.selenide.SelenideElement;
import domain.web.frontendlayer.pages.AbstractPage;
import models.AuthenticationModel;

import static com.codeborne.selenide.Selenide.$;

public class LoginPage extends AbstractPage {
    private SelenideElement usernameInput = $("#username"),
            passwordInput = $("#password"),
            loginBtn = $("#Login");

    public LoginPage setName(AuthenticationModel authenticationModel) {
        usernameInput.setValue(authenticationModel.getUsername());
        return new LoginPage();
    }

    public LoginPage setPassword(AuthenticationModel authenticationModel) {
        passwordInput.setValue(authenticationModel.getPassword());
        return new LoginPage();
    }

    public SetupHomePage clickSingIn() {
        loginBtn.click();
        return new SetupHomePage();
    }
}