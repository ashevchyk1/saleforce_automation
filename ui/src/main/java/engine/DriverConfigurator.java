package engine;

import com.codeborne.selenide.Configuration;
import io.github.bonigarcia.wdm.WebDriverManager;

import java.time.Duration;

import static org.assertj.core.api.Fail.fail;

public final class DriverConfigurator {
    private static final long IMPLICIT_TIMEOUT = Duration.ofSeconds(20).toMillis();

    public static void configure() {
        Configuration.timeout = IMPLICIT_TIMEOUT;
        Configuration.startMaximized = true;
        Configuration.savePageSource = Boolean.valueOf(System.getProperty("selenide.savePageSource", "false"));
        DriverType driver = DriverType.getByName(System.getProperty("driver.type"));
        switch (driver) {
            case CHROME:
                WebDriverManager.chromedriver().setup();
                Configuration.browser = ChromeDriverProvider.class.getName();
                break;
            default:
                fail("Provide implementation for " + driver + " driver");
                break;
        }
    }
}
