package engine;

import java.util.Arrays;
import java.util.Optional;

public enum DriverType {
    CHROME;

    public static DriverType getByName(String driverType){
        Optional o = Arrays.stream(values()).filter(e -> e.name().equals(driverType)).findFirst();
        if (o.isPresent()) {
            return (DriverType) o.get();
        }
        throw new RuntimeException("'" + driverType + "' is not defined in 'DriverType' enum");
    }
}
