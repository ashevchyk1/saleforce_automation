package engine;

import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public final class DriverUtils {

    public static void stop() {
        WebDriverRunner.closeWebDriver();
    }

    public static byte[] getScreenshot() {
        if (WebDriverRunner.hasWebDriverStarted()) {
            return ((TakesScreenshot) WebDriverRunner.getWebDriver()).getScreenshotAs(OutputType.BYTES);
        } else {
            return new byte[]{};
        }
    }
}
