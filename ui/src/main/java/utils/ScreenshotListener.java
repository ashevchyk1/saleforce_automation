package utils;

import engine.DriverUtils;
import io.qameta.allure.Attachment;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class ScreenshotListener extends TestListenerAdapter {

    @Override
    public void onTestFailure(ITestResult result) {
        takeScreenshot(result.getMethod().getMethodName());
    }

    @Attachment("{0}")
    private byte[] takeScreenshot(String name) {
        return DriverUtils.getScreenshot();
    }
}
